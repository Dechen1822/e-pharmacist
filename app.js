const express = require("express")
const path = require('path')
const app = express()
const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
const newsRouter = require('./routes/newsRoutes')
const cookieParser = require('cookie-parser')


app.use(cookieParser())
app.use(express.json())
app.use('/api/v1/users', userRouter)
app.use('/api/v1/news', newsRouter)
app.use('/', viewRouter)

app.use(express.static(path.join(__dirname, 'views')))
module.exports = app
