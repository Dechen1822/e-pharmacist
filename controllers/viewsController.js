const path = require("path");

//Login page

exports.getLoginForm = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views", "login.html"));
};

//Sign up page

exports.getSignupForm = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views", "signup.html"));
};

// Home Page
exports.getHome = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views", "dashboard.html"));
};


exports.getProfile = (req, res) => {
  res.sendFile(path.join(__dirname, '../', 'views', 'myprofilepage.html'))
  
}