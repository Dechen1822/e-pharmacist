const User = require('./../models/userModels')
const AppError = require('./../utils/appError')
const multer = require('multer')

const multerStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'views/img/');
  },
  filename: (req, file, cb) => {

    var obj = JSON.parse(req.cookies.token)
    const ext = file.mimetype.split('/')[1];
    cb(null, `user-${obj['_id']}-${Date.now()}.${ext}`);
  }
});
// const multerStorage = multer.memoryStorage();

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith('image')) {
    cb(null, true);
  } else {
    cb(new AppError('Not an image! Please upload only images.', 400), false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter
});

exports.uploadUserPhoto = upload.single('photo');



const filterObj = (obj, ...allowedFields) => {
    const newObj = {}
    Object.keys(obj).forEach((el) => {
        if (allowedFields.includes(el)) newObj[el] = obj[el]
    })
    return newObj
}

exports.updateMe = async (req, res, next) => {

    try{
    // 1) Create error if user POSTs password data
    if (req.body.password || req.body.passwordConfirm) {
        return next(
          new AppError(
            'This route is not for password updates. Please use /updateMyPassword.',
            400
          )
        );
      }
  
      const filteredBody = filterObj(req.body, 'name', 'email')
      if (req.body.photo !== 'undefined'){
          filteredBody.photo = req.file.filename
      }
      var obj = JSON.parse(req.cookies.token)   
      const updatedUser = await User.findByIdAndUpdate(obj['_id'], filteredBody, {
        new: true,
        runValidators: true
      });
    
      res.status(200).json({
        status: 'success',
        data: {
          user: updatedUser
        }
      });

    }catch (err) {
        res.status(500).json({ error: err.message});
    }
  };

exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await User.find();
        res.status(200).json({data: users, status: 'Success'});

    } catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.createUsers = async (req, res) => {
    try {
        const users = await User.create(req.body);
        console.log(req.body.name);
        res.json({data: users, status: 'Success'});

    } catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.getUsers = async (req, res) => {
    try {
        const users = await User.findById(req.params.id);
        res.json({data: users, status: 'Success'});

    } catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.updateUsers = async (req, res) => {
    try {
        const users = await User.findByIdAndUpdate(req.params.id);
        res.json({data: users, status: 'Success'});

    } catch (err) {
        res.status(500).json({error: err.message});
    }
}

exports.deleteUsers = async (req, res) => {
    try {
        const users = await User.findByIdAndDelete(req.params.id);
        res.json({data: users, status: 'Success'});

    } catch (err) {
        res.status(500).json({error: err.message});
    }
}